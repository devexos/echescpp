#include "Pieces/Piece.hpp"

class Case {
  Piece *piece;

  public:
    Case ();

    ~Case();

    void setPiece(Piece* p);

    void retirePiece();

    Piece* getPiece();

    bool estVide();

    string toString();

};