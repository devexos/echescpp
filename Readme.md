Pour compiler, il faut prendre tous les .cpp (même dans les sous-répertoires) :

`g++ **/*.cpp -o main.exe --std=c++11`
