#include "Plateau.hpp"

void Plateau::afficheNumeroColonnes() {
  cout << "  ";
  for (int i = 0; i < NB_COLONNES; i++)
  {
    cout << i << " ";
  }
  cout << endl;
}

void Plateau::afficheLigne(int ligne) {
  cout << ligne << " ";
  for (int i = 0; i < NB_COLONNES; i++)
  {
    cout << cases[ligne][i].toString() << " ";
  }
  cout << endl;
}
bool Plateau::estCaseExistante(Position pos) {
  return pos.getLigne() >= 0 && pos.getLigne() < NB_LIGNES
          && pos.getColonne() >= 0 && pos.getColonne() < NB_COLONNES;
}


Plateau::Plateau() {
  initPieces();
}

void Plateau::initPieces() {
  for (int i = 0; i < NB_COLONNES; i++)
  {
    getCase(Position(1,i)).setPiece(new Pion(false));
    getCase(Position(6,i)).setPiece(new Pion(true));
  }

  getCase(Position(0,1)).setPiece(new Cavalier(false));
  getCase(Position(7,1)).setPiece(new Cavalier(true));

  getCase(Position(0,2)).setPiece(new Fou(false));
  getCase(Position(7,2)).setPiece(new Fou(true));

}

Case& Plateau::getCase(Position pos) {
  int ligne = pos.getLigne();
  int colonne = pos.getColonne();
  return cases[ligne][colonne];
}

void Plateau::affiche() {
  afficheNumeroColonnes();
  for(int i = 0; i < NB_LIGNES; i++) {
    afficheLigne(i);
  }
}


bool Plateau::joue(Position initiale, Position finale) {
  if (!estCaseExistante(initiale) || !estCaseExistante(finale)) {
    return false;
  }
  Piece * piece = getCase(initiale).getPiece();
  if (!getCase(finale).estVide()) { // on va prendre une pièce
    if (getCase(finale).getPiece()->estBlanche() == piece->estBlanche()) {
      return false;
    }
    if (piece->peutSeDeplacerPourPrendre(initiale, finale)) {
      getCase(initiale).retirePiece();
      getCase(finale).setPiece(piece);
      return true;
    }
  } else {
    if (piece->peutSeDeplacer(initiale, finale)) {
      getCase(initiale).retirePiece();
      getCase(finale).setPiece(piece);
      return true;
    }
  }
  return false;
}
