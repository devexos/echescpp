#include "Jeu.hpp"

void Jeu::changeTour() {
  tourBlancs = !tourBlancs;
}

Jeu::Jeu () {
  tourBlancs = true;
}
void Jeu::affiche() {
  plateau.affiche();
  if (tourBlancs) {
    cout << "Au tour des blancs !" << endl;
  } else {
    cout << "Au tour des noirs !" << endl;
  }
}

bool Jeu::joue(Position initiale, Position finale){
  if (plateau.joue(initiale, finale)) {
    changeTour();
    return true;
  }
  return false;
}
