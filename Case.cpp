#include "Case.hpp"

Case::Case () {
  piece = nullptr;
}

Case::~Case() {
  if (this->piece != nullptr) {
    delete this->piece;
  }
}

void Case::setPiece(Piece* p) {
  if (this->piece != nullptr) {
    delete this->piece;
  }
  this->piece = p;
}

void Case::retirePiece() {
  this->piece = nullptr;
}

Piece* Case::getPiece() {
  return this->piece;
}

bool Case::estVide() {
  return piece == nullptr;
}

string Case::toString() {
  if (!estVide())
    return this->piece->getLettre();
  return "_";
}
