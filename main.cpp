#include <iostream>
#include <cstdlib>

using namespace std;

#include "Jeu.hpp"


int main(int argc, char const *argv[]) {
  Jeu jeu;
  jeu.affiche();
  jeu.joue(Position(7,2),Position(5,4));
  jeu.affiche();
  jeu.joue(Position(1,0),Position(3,0));
  jeu.affiche();
  jeu.joue(Position(7,1),Position(4,0));
  jeu.affiche();
  jeu.joue(Position(7,1),Position(5,0));
  jeu.affiche();
  jeu.joue(Position(3,0),Position(4,0));
  jeu.affiche();
  jeu.joue(Position(5,0),Position(3,-1));
  jeu.affiche();
  jeu.joue(Position(6,2),Position(4,2));
  jeu.affiche();
  jeu.joue(Position(1,1),Position(3,1));
  jeu.affiche();
  jeu.joue(Position(6,1),Position(4,1));
  jeu.affiche();
  jeu.joue(Position(3,1),Position(4,1));
  jeu.affiche();
  jeu.joue(Position(3,1),Position(4,2));
  jeu.affiche();
  jeu.joue(Position(5,0),Position(4,2));
  jeu.affiche();

  return 0;
}