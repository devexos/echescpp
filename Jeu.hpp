#include "Plateau.hpp"
#include "Position.hpp"

class Jeu {

  Plateau plateau;
  bool tourBlancs;

  void changeTour() ;

  public:
    Jeu();

    void affiche();
    bool joue(Position initiale, Position finale);

};