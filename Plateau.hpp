#include "Pieces/Piece.hpp"
#include "Pieces/Pion.hpp"
#include "Pieces/Cavalier.hpp"
#include "Pieces/Fou.hpp"
#include "Case.hpp"

class Plateau {

  const static int NB_LIGNES = 8;
  const static int NB_COLONNES = 8;

  Case cases[NB_LIGNES][NB_COLONNES];

  void afficheNumeroColonnes();
  void afficheLigne(int ligne);
  bool estCaseExistante(Position pos);


  public:
    Plateau();

    void initPieces();
    Case& getCase(Position pos);
    void affiche();
    bool joue(Position initiale, Position finale);
};