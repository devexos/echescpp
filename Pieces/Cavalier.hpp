#pragma once
#include "Piece.hpp"

class Cavalier : public Piece {
  public:
    Cavalier(bool estBlanc);
    string getLettre();
    bool peutSeDeplacer(Position initiale, Position finale);
};
