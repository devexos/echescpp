#pragma once
#include "../Position.hpp"
#include <iostream>

using namespace std;


class Piece {
  bool _estBlanche = true;

  public: 
    Piece(bool estBlanche);
    virtual ~Piece();

    virtual bool estBlanche();

    virtual string getLettre() = 0;

    virtual bool peutSeDeplacer(Position initiale, Position finale);
    virtual bool peutSeDeplacerPourPrendre(Position initiale, Position finale);
};