#include "Piece.hpp"

Piece::Piece(bool estBlanche) {
  this->_estBlanche = estBlanche;
}
Piece::~Piece() {}

bool Piece::estBlanche() {
  return _estBlanche;
}

bool Piece::peutSeDeplacer(Position initiale, Position finale) {
  return false;
}
bool Piece::peutSeDeplacerPourPrendre(Position initiale, Position finale) {
  return this->peutSeDeplacer(initiale, finale);
}
