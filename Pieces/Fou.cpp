#include "Fou.hpp"

Fou::Fou(bool estBlanc) : Piece(estBlanc) {}

string Fou::getLettre() {
  return estBlanche() ? "F" : "f";
}

bool Fou::peutSeDeplacer(Position initiale, Position finale) {
  Position delta = initiale.difference(finale);
  return abs(delta.getLigne()) == abs(delta.getColonne());
}
