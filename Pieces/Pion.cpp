#include "Pion.hpp"

Pion::Pion(bool estBlanc) : Piece(estBlanc) {}

Pion::~Pion() {}

string Pion::getLettre() {
  return estBlanche() ? "P" : "p";
}
bool Pion::peutSeDeplacer(Position initiale, Position finale) {
  Position delta = finale.difference(initiale);
  if (delta.getColonne() != 0)
    return false;
  if (delta.getLigne() == 0)
    return false;
  if (delta.getLigne() < 0 && !estBlanche()) {
    return false;
  }
  if (delta.getLigne() > 0 && estBlanche()) {
    return false;
  }
  if (abs(delta.getLigne()) == 1) {
    aBouge = true;
    return true;
  }
  if (!aBouge && abs(delta.getLigne()) == 2) {
    aBouge = true;
    return true;
  }
  return false;
  
}

bool Pion::peutSeDeplacerPourPrendre(Position initiale, Position finale) {
  Position delta = initiale.difference(finale);
  return abs(delta.getLigne()) == abs(delta.getColonne()) 
    &&  abs(delta.getColonne()) == 1;

}

