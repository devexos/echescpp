#include "Piece.hpp"

class Fou : public Piece {
  public:
    Fou(bool estBlanc);
    string getLettre();
    bool peutSeDeplacer(Position initiale, Position finale);
};
