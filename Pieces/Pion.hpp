#pragma once

#include "Piece.hpp"

#include <iostream>

using namespace std;


class Pion : public Piece {
  bool aBouge = false;
  public:
    Pion(bool estBlanc);
    ~Pion();
    string getLettre();
    bool peutSeDeplacer(Position initiale, Position finale);

    bool peutSeDeplacerPourPrendre(Position initiale, Position finale);
};