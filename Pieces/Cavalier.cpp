#include "Cavalier.hpp"

Cavalier::Cavalier(bool estBlanc) : Piece(estBlanc) {}

string Cavalier::getLettre() {
  return estBlanche() ? "C" : "c";
}

bool Cavalier::peutSeDeplacer(Position initiale, Position finale) {
  Position delta = initiale.difference(finale);
  return abs(delta.getLigne()) * abs(delta.getColonne()) == 2;
}
